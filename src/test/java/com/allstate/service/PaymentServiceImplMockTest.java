package com.allstate.service;

import com.allstate.assessment.customexception.OutOfRangeException;
import com.allstate.assessment.dao.PaymentRepo;
import com.allstate.assessment.dto.entities.Payment;
import com.allstate.assessment.service.PaymentService;
import com.allstate.assessment.service.PaymentServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.isA;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PaymentServiceImplMockTest {

    @Mock
    private PaymentRepo paymentRepo ;


    @Test
    public void testRowCount(){
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        when(paymentRepo.rowcount()).thenReturn(3);
        int count = paymentService.rowcount();
        System.out.println(count);
        assertTrue(count>0);

    }

    @Test
    public void testFindByIdThrowsException() throws OutOfRangeException {
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        assertThrows(OutOfRangeException.class,()->{ Payment payment = paymentService.findById(0);});

    }

    @Test
    public void testFindByIdReturnsPayment() throws OutOfRangeException{
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo) ;
        Date date = new Date();
        Payment payment = new Payment(1,date,"cash",200.0,123);
        when(paymentRepo.findById(ArgumentMatchers.anyInt())).thenReturn(payment);
        Payment payment1 =paymentService.findById(3);
        System.out.println(payment1.getAmount());
        assertThat(payment1, isA(Payment.class));

        }

    @Test
    public void testFindByTypeReturnsException (){
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        assertThrows(OutOfRangeException.class,()->{ List<Payment> payments = paymentService.findByType("");});

    }

    @Test
    public void testFindByType() throws OutOfRangeException {
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        List<Payment> payments = new ArrayList<>();
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "cash", 200.0, 123);
        Payment payment2 = new Payment(2, date, "cash", 250.0, 124);
        Payment payment3 = new Payment(3, date, "cash", 300.0, 125);
        payments.add(payment1);
        payments.add(payment2);
        payments.add(payment3);
        when(paymentRepo.findByType(ArgumentMatchers.anyString())).thenReturn(payments);
        List<Payment> payments1 = paymentService.findByType("cash");
        System.out.println(payments1.get(1).getAmount());
        assertTrue(payments1.size() > 0);
    }
        @Test
        public void testSaveReturnIntValue() throws OutOfRangeException{
            PaymentService paymentService = new PaymentServiceImpl(paymentRepo) ;
            Date date = new Date();
            Payment payment1 = new Payment(1,date,"cash",200.0,123);
            int result = paymentService.save(payment1);
            assertTrue(result!=-1);
        }
    }




