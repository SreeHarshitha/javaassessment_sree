package com.allstate.dao;

import com.allstate.assessment.dao.PaymentDAO;
import com.allstate.assessment.dao.PaymentDAOMySQL;
import com.allstate.assessment.dao.PaymentRepo;
import com.allstate.assessment.dto.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentRepoTest {
    PaymentRepo paymentRepo;
    @BeforeEach
    void setUp() {
        paymentRepo = new PaymentDAO();
                    //  new PaymentDAOMySQL();
    }
   /* @Test
    void testisConnected(){
        assertTrue(((PaymentDAOMySQL)paymentRepo).isConnected());
    }*/

    @Test
    void testsave(){
        Date date = new Date();
      int result = paymentRepo.save(new Payment(0,date,"card",300.0,133));
      assertTrue(result>0);
    }

    @Test
    void testrowcount(){
        int result = paymentRepo.rowcount();
        assertTrue(result!=-1);
    }
    @Test
    void testFindById(){
        Payment payment = paymentRepo.findById(1);
        assertNotNull(payment);

    }
    @Test
    void testFindByType(){
        List<Payment> paymentList = new ArrayList<>();
        paymentList = paymentRepo.findByType("cash");
        assertNotNull(paymentList);

    }

}
