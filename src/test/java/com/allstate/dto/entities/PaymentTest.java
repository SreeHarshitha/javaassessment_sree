package com.allstate.dto.entities;

import com.allstate.assessment.dto.entities.Payment;
import org.junit.jupiter.api.Test;


import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTest {
   @Test
    void paymentTest(){
        Date date = new Date();//LocalDate.parse("2021-04-09");
        Payment payment = new Payment(1, date,"cash",100,123);
      assertEquals(1,payment.getId());
    }
    @Test
    void toStringTest(){
        Date date = new Date();
        System.out.println("date: "+date);//LocalDate.parse("2021-04-09");
        Payment payment = new Payment();
        payment.setId(1);
        payment.setPaymentdate(date);
        payment.setAmount(200);
        payment.setType("card");
        payment.setCustid(456);
        assertEquals("Payment{" +"id=" + 1 +", paymentdate=" + date +", type='" +
                "" + "card" + '\'' +", amount=" + 200.0 +", custid=" + 456 +'}',payment.toString());

    }


}
