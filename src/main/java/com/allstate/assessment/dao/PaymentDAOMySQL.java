package com.allstate.assessment.dao;

import com.allstate.assessment.dto.entities.Payment;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PaymentDAOMySQL implements PaymentRepo {
    private Connection connection;
    public PaymentDAOMySQL() {
        getConnection();
    }

    private  Connection getConnection() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/training?serverTimezone=UTC&useSSL=false",
                    "root", "c0nygre");
            //appication.properties

        } catch (ClassNotFoundException | SQLException e ) {
            e.printStackTrace();
        }
        return this.connection;
    }

    public boolean isConnected()
    {

        Statement statement= null;
        try {

            statement = this.connection.createStatement();
            ResultSet rs=statement.executeQuery("select 1");
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }


    }


    @Override
    public int rowcount() {
        int result;
        Statement statement;
        try{
            statement= this.connection.createStatement();
            ResultSet rs =statement.executeQuery("SElect COUNT(*) From Payment");


            return rs.getRow();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            return -1;
        }

    }

    @Override
    public Payment findById(int id) {
        try {
            PreparedStatement preparedStatement = this.connection.
                    prepareStatement("SElect * From Payment where id=?");

            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.first();
            Payment payment = new Payment(
                    resultSet.getInt("id"),
                    resultSet.getDate("paymentdate"),
                    resultSet.getString("paymenttype"),
                    resultSet.getDouble("amount"),
                    resultSet.getInt("custid")

            );
            System.out.println(payment);
            return payment;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }
    @Override
    public List<Payment> findByType(String type) {
        List<Payment> paymentList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connection.
                    prepareStatement("SElect * From Payment where paymenttype=?");

            preparedStatement.setString(1, type);
            ResultSet resultSet = preparedStatement.executeQuery();
            //resultSet.first();
           while(resultSet.next()) {
               Payment payment = new Payment(
                       resultSet.getInt("id"),
                       resultSet.getDate("paymentdate"),
                       resultSet.getString("paymenttype"),
                       resultSet.getDouble("amount"),
                       resultSet.getInt("custid")

               );
               paymentList.add(payment);
               System.out.println(payment);
           }

            return paymentList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        //return null;
    }

    @Override
    public int save(Payment payment) {
        try {
            PreparedStatement preparedStatement= this.connection.prepareStatement("Insert into Payment(paymentdate, paymenttype, amount,custid) values (?,?,?,?)");
            preparedStatement.setDate(1, new java.sql.Date(((java.util.Date) payment.getPaymentdate()).getTime()));
            preparedStatement.setString(2, payment.getType());
            preparedStatement.setDouble(3, payment.getAmount());
            preparedStatement.setInt(4,payment.getCustid());
            return preparedStatement.executeUpdate();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return  -1;
        }
    }
}
