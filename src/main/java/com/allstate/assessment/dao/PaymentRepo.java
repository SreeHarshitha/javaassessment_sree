package com.allstate.assessment.dao;

import com.allstate.assessment.dto.entities.Payment;

import java.util.List;

public interface PaymentRepo {

    int rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type );
    int  save(Payment payment);

}
