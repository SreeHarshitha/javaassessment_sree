package com.allstate.assessment.dao;

import com.allstate.assessment.dto.entities.Payment;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import static com.mongodb.client.model.Filters.*;
import java.util.*;
import com.mongodb.client.MongoCursor;



public  class PaymentDAOMongo implements PaymentRepo {

    private MongoClient mymongoclient;
    private MongoDatabase database;
    private MongoCollection<Document> collection;


    public PaymentDAOMongo() {
        mymongoclient = new MongoClient("localhost", 27017);
        database = mymongoclient.getDatabase("training");
        collection = database.getCollection("payment");

    }
    public  int rowcount(){
        FindIterable<Document> findIterable = collection.find(new Document());
        MongoCursor<Document> cursor = findIterable.iterator();
        int count=0;
        try {
            while(cursor.hasNext()) {
                cursor.next();
                count++;
            }
        } finally {
            cursor.close();
        }
        return count;
    }

    public Payment findById(int id){
        Payment payment = new Payment();
        try {

            Document mydoc = collection.find().first();
            payment.setId((Integer) mydoc.get("id"));
            payment.setType(mydoc.getString("type"));
            payment.setPaymentdate((Date) mydoc.get("paymentdate"));
            payment.setAmount(Double.valueOf(mydoc.get("amount").toString()).doubleValue());
            payment.setCustid((Integer) mydoc.get("custid"));
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return payment;
    }
    public  List<Payment>  findByType(String type ){
        List<Payment> paymentList = new ArrayList<Payment>();
        try {
            FindIterable<Document> findType = collection.find(eq("type", "cash"));
            findType.forEach(new Block<Document>() {
                Payment payment = new Payment();

                @Override
                public void apply(Document document) {
                    payment.setId((Integer) document.get("id"));
                    payment.setType(document.getString("type"));
                    payment.setPaymentdate((Date) document.get("paymentdate"));
                    payment.setAmount(Double.valueOf(document.get("amount").toString()).doubleValue());
                    payment.setCustid((Integer) document.get("custid"));
                    paymentList.add(payment);
                }
            });
            System.out.println(paymentList.size());
        }catch(Exception e){

            e.printStackTrace();
        }
        return paymentList;
    }


    public  int  save(Payment payment){
        Date date = new Date();
        Document newDoc = new Document("id", payment.getId())
                .append("type", "cash")
                .append("paymentdate", date)
                .append("amount",payment.getAmount())
                .append("custid",payment.getCustid());

        collection.insertOne(newDoc);
        return 1;
    }


}
