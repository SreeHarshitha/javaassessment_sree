package com.allstate.assessment.service;

import com.allstate.assessment.customexception.OutOfRangeException;
import com.allstate.assessment.dto.entities.Payment;

import java.util.List;

public interface PaymentService {
    int rowcount();
    Payment findById(int id) throws OutOfRangeException;
    List<Payment> findByType(String type) throws OutOfRangeException;
    int save(Payment payment) throws OutOfRangeException;
}
