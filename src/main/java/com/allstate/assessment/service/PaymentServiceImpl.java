package com.allstate.assessment.service;

import com.allstate.assessment.customexception.OutOfRangeException;
import com.allstate.assessment.dao.PaymentRepo;
import com.allstate.assessment.dto.entities.Payment;

import java.util.List;

public class PaymentServiceImpl implements PaymentService{
    private PaymentRepo paymentRepo;
    public PaymentServiceImpl(PaymentRepo paymentRepo){
        this.paymentRepo = paymentRepo;
    }
    @Override
    public int rowcount() {
        return paymentRepo.rowcount();
    }

    @Override
    public Payment findById(int id) throws OutOfRangeException {
        if(id<1){
            throw new OutOfRangeException("Id should be greater than 1");
        }
        return paymentRepo.findById(id);
    }

    @Override
    public List<Payment> findByType(String type) throws OutOfRangeException {
        if(type.equals("")){
            throw new OutOfRangeException("Type cant be null");
        }
        return paymentRepo.findByType("cash");


    }

    @Override
    public int save(Payment payment) throws OutOfRangeException {
    if(payment.getId()<=0){
        throw new OutOfRangeException("Id should be a positive integer");
    }
        return paymentRepo.save(payment);
    }
}
